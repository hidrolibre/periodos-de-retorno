!****************************************************************************
!
!  Programa:    FDP
!  Subrutina:   SeriesSinteticas
!  Comentarios: Ajusta la serie a FDP por momentos
!  Autor:       Jos� Luis Arag�n Hern�ndez
!  FechaIni:    Julio/2016
!  FechaFin:    Julio/2016
!
!****************************************************************************

    subroutine SerieSintetica 
    
    use moduloLeeDat
    use moduloParEst
    use moduloPruCal   
    use moduloSerSin
    
    implicit real*8 (a-h,o-z)
    include 'AHF_Commons.txt' 
    
    character(len=200)text
    character(len=100)pei,pein

    real*8:: cia(2)
    allocate (zt(-1:ndat+2,nms),yt(ndat,nms),xs(ndat,nms),xmeds(nms),xvars(nms),xdests(nms),cass(nms))
    zt=0
    yt=0
    xs=0
    xmeds=0
    xvars=0
    xdests=0
    cass=0
   
    write(1,*)'Inicia c�lculo de series sint�ticas' 
    write(3,*)
    write(3,*)'Serie original'
    do id=1,ndat
      write(3,1)xori(id)
    end do 
    write(3,2)' Media:                  ',xmed
    write(3,2)' Varianza:               ',xvar
    write(3,2)' Desviaci�n estandar:    ',xdest
    write(3,2)' Coeficiente de asimetr�a',casn   

    write(3,*)
    write(3,4)' Prueba estad�stica de Anderson: ',pei
    write(3,*)'    rk(+95)          rk     rk(-95)'
    do ik=1,nk
      write (3,1)rk1(ik),rk(ik),rk2(ik) 
    end do    
    
    write(3,*)
    write(3,*)'Serie normalizada'
    do id=1,ndat
      write(3,1)xn(id)
    end do 
    write(3,2)' Media:                  ',xmedn
    write(3,2)' Varianza:               ',xvarn
    write(3,2)' Desviaci�n estandar:    ',xdestn
    write(3,2)' Coeficiente de asimetr�a',casn           
    write(3,1)
    write(3,4)' Prueba estad�stica de Anderson: ',pei
    write(3,*)'    rk(+95)          rk     rk(-95)'
    do ik=1,nk
      write (3,1)rkn1(ik),rkn(ik),rkn2(ik) 
    end do    

!***Modelo AR(p)    
    cia(1)=ndat*log(xve)+2*1
    cia(2)=ndat*log(xve)+2*2
    write(3,*)
    if (cia(1).lt.cia(2)) then 
      write(3,*)'Modelo AR(1)'
    else
      write(3,*)'Modelo AR(2)'
    end if  
    do ims=1,nms
!***Modelo AR(1)   
      if (cia(1).lt.cia(2)) then 
      fi1=rkn(1)
        xve=(ndat/(ndat-1.))*xvarn*(1-fi1**2.)
        do id1=1,ndat+1,2
          call random_seed()         
          call random_number(al1)
          do i=1,1000000
            a1=1
            a2=2
            b1=a1+a2
            b2=a1+a2
            b3=a1*a2 
            b4=a1/a2 
          end do
          call random_seed()
          call random_number(al2)      
          e1=sqrt(2.*log(1./al1))*cos((2*pi*al2)*(180/180.))
          e2=sqrt(2.*log(1./al1))*sin((2*pi*al2)*(180/180.))
          zt(id1,ims)=fi1*zt(id1-1,ims)+xve*e1
          zt(id1+1,ims)=fi1*zt(id1,ims)+xve*e2
        end do
!***Modelo AR(2)      
      else
        fi1=(rkn(1))*(1.-rkn(2))/(1.-rkn(1)**2.)
        fi2=(rkn(2)-rkn(1)**2.)/(1.-rkn(1)**2.)
        xve=(ndat/(ndat-2.))*xvarn*((1.+fi2)/(1.-fi2))*((1-fi2)**2.-fi1**2.)
        do id1=1,ndat+1,2
          call random_seed()         
          call random_number(al1)
          do i=1,1000000
            a1=1
            a2=2
            b1=a1+a2
            b2=a1+a2
            b3=a1*a2 
            b4=a1/a2 
          end do
          call random_seed()
          call random_number(al2) 
          e1=sqrt(2*log(1./al1))*cos((2*pi*al2)*(180/180))
          e2=sqrt(2*log(1./al1))*sin((2*pi*al2)*(180/180))
          zt(id1,ims)=fi1*zt(id1-1,ims)+fi2*zt(id1-2,ims)+xve*e1
          zt(id1+1,ims)=fi1*zt(id1,ims)+fi2*zt(id1-1,ims)+xve*e2
        end do   
      end if
      sumxs=0
      do id=1,ndat
        yt(id,ims)=zt(id,ims)+xmedn
        xs(id,ims)=exp(yt(id,ims))
        sumxs=sumxs+xs(id,ims)
      end do
      xmeds(ims)=sumxs/ndat
      sumxs2=0
      sumxs3=0
      do id=1,ndat
        sumxs2=sumxs2+(xs(id,ims)-xmeds(ims))**2.
        sumxs3=sumxs3+(xs(id,ims)-xmeds(ims))**3.
      end do 
      xvars(ims)=sumxs2/(ndat-1)   
      xdests(ims)=sqrt(xvars(ims))
      cass(ims)=(sumxs3/ndat)/(sumxs2/ndat)**1.5 
    end do 

    do id=1,ndat
      !write(3,1)zt(id,1:nms)
    end do
    
    !write(3,*)'*******************'    
    do id=1,ndat
      !write(3,2)yt(id,1:nms)
    end do
    
    !write(3,*)'*******************'    
    do id=1,ndat
      write(3,1)xs(id,1:nms)
    end do
    text=' Media, Varianza, Desviaci�n estandar, Coeficiente de asimetr�a'
    write(3,3)text
    write(3,1)xmeds(1:nms)
    write(3,1)xvars(1:nms)
    write(3,1)xdests(1:nms)
    write(3,1)cass(1:nms)   

1   format(20f12.3)
2	format(a,20f12.3)
3	format(a100)   
4	format(a,a)   

    write(1,*)'Termina c�lculo de series sint�ticas'   
    
    end  subroutine SerieSintetica 