!****************************************************************************
!
!  Programa:    FDP
!  Subrutina:   Momentos
!  Comentarios: Ajusta la serie a FDP por momentos
!  Autor:       Jos� Luis Arag�n Hern�ndez
!  FechaIni:    Abr/2016
!  FechaFin:    Abr/2016
!
!****************************************************************************

    subroutine Momentos 
    use modulo1     
    use modulo2
    use modulo3
    
    implicit real*8 (a-h,o-z)
    
    common/dat1/ndat,ntr,nf,nk
    common/dat2/xmed,xvar,xdest,xml,xdel,cas,casl

    write(2,*)'Inicia c�lculo de par�metros por el m�todo de momentos'

    nf=8.
    allocate (xt(ndat,nf),xtt(ntr,nf),er2(ndat,nf),eea(nf),pe(3,nf))
    xt=0.
    xtt=0.
    er2=0.
    eea=0.
    pe=-9999.
!***Momentos****************************************************************************
!***Gamma2P***************************************
    pe(1,1)=xdest**2./xmed
    pe(2,1)=(xmed/xdest)**2.
    sume=0.
    do id=1,ndat
      call cal1 (id,ut)
      xt(id,1)=pe(1,1)*pe(2,1)*(1.-1./(9.*pe(2,1))+ut*sqrt(1./(9.*pe(2,1))))**3.
      er2(id,1)=(xt(id,1)-xord(id))**2.
      sume=sume+er2(id,1)
    end do 

    eea(1)=sqrt(sume/(ndat-2.))
    
    do it=1,ntr
      call cal2 (it,ut)
      xtt(it,1)=pe(1,1)*pe(2,1)*(1.-1./(9.*pe(2,1))+ut*sqrt(1./(9.*pe(2,1))))**3.
    end do      
      
!***Gamma 3P***************************************
    pe(1,2)=4./cas**2.
    pe(2,2)=xdest/sqrt(pe(1,2))
    pe(3,2)=xmed-xdest*sqrt(pe(1,2))
    sume=0. 
    do id=1,ndat
      call cal1 (id,ut)
      xt(id,2)=pe(3,2)+pe(2,2)*pe(1,2)*(1.-1./(9*pe(1,2))+ut*sqrt(1./(9.*pe(1,2))))**3.
      er2(id,2)=(xt(id,2)-xord(id))**2.
      sume=sume+er2(id,2)
    end do     
    
    eea(2)=sqrt(sume/(ndat-3.))

    do it=1,ntr
      call cal2 (it,ut)
      xtt(it,2)=pe(3,2)+pe(2,2)*pe(1,2)*(1.-1./(9*pe(1,2))+ut*sqrt(1./(9.*pe(1,2))))**3.
    end do   
 
 !***General de Valores Extremos 3P**********************   
    if (cas.lt.1.1396) then
      pe(1,3)=0.279434-0.333535*cas+0.048306*cas**2.+0.023314*cas**3.+0.003670*cas**4.+0.000263*cas**5.
    else
      pe(1,3)=0.250310-0.292190*cas+0.075357*cas**2.+0.010883*cas**3.+0.000904*cas**4.+0.000043*cas**5.
    end if
    g1b=0.91457                                                                                       !Revisar como se obtiene
    g2b=0.88609
    g21b=g1b**2.
    s2y=g2b-g21b
    bf=sqrt(xvar/s2y)
    af=xmed+bf*g1b
    if (pe(1,3).gt.0.) then
      pe(2,3)=pe(1,3)*bf
      pe(3,3)=af-bf
    else
      pe(2,3)=-pe(1,3)*bf
      pe(3,3)=af+bf
    end if
    sume=0. 
    do id=1,ndat
    a1=1-pexc(id)
    a2=log(a1)
    a3=1-(-a2)
    
    a4=a3**pe(1,3)
    xt(id,3)=pe(3,3)+(pe(2,3)/pe(1,3))*a4
      xt(id,3)=pe(3,3)+(pe(2,3)/pe(1,3))*(1-(-log(1-pexc(id)))**pe(1,3))
      er2(id,3)=(xt(id,3)-xord(id))**2.
      sume=sume+er2(id,3)
    end do    
    
    eea(3)=sqrt(sume/(ndat-3.))

    do it=1,ntr
      xtt(it,3)=pe(3,3)+(pe(2,3)/pe(1,3))*(1-(-log(1-1./test(it)))**pe(1,3))
    end do   
   
!***Gumbel 2P***************************************
    pe(1,4)=xmed-0.45*xdest   
    pe(2,4)=0.785*xdest 
    sume=0.
    do id=1,ndat
      xt(id,4)=pe(1,4)-pe(2,4)*log(-log(1.-pexc(id)))
      er2(id,4)=(xt(id,4)-xord(id))**2.
      sume=sume+er2(id,4)
    end do  
    
    eea(4)=sqrt(sume/(ndat-2.))
    
    do it=1,ntr
      xtt(it,4)=pe(1,4)-pe(2,4)*log(-log(1.-1./test(it)))
    end do     

!***LogNormal 2P***************************************
    pe(1,5)=xml
    pe(2,5)=xdel
    sume=0.
    do id=1,ndat
      call cal1 (id,ut)
      xt(id,5)=exp(pe(1,5)+pe(2,5)*ut)
      er2(id,5)=(xt(id,5)-xord(id))**2.
      sume=sume+er2(id,5)
    end do 
    
    eea(5)=sqrt(sume/(ndat-2.))
    
    do it=1,ntr
      call cal2 (it,ut)
      xtt(it,5)=exp(pe(1,5)+pe(2,5)*ut)
    end do  
    
!***LogNormal 3P***************************************
    aux1=xdest/xmed
    aux2=((cas**2.+4.)**0.5-cas)/2.
    aux3=(1-aux2**(2./3.))/aux2**(1./3.)
    pe(1,6)=xmed*(1-(aux1/aux3))
    pe(2,6)=log(xdest/aux3)-0.5*log(aux3**2.+1)
    pe(3,6)=(log(aux3**2.+1))**0.5
    sume=0.  
    do id=1,ndat
      call cal1 (id,ut)
      xt(id,6)=pe(1,6)+exp(pe(2,6)+ut*pe(3,6))
      er2(id,6)=(xt(id,6)-xord(id))**2.
      sume=sume+er2(id,6)
    end do 
    
    eea(6)=sqrt(sume/(ndat-3.))
    
    do it=1,ntr
      call cal2 (it,ut)
      xtt(it,6)=pe(1,6)+exp(pe(2,6)+ut*pe(3,6)) 
    end do             
        
!***LogPerarson 2P III***************************************
    pe(1,7)=4./casl**2.
    pe(2,7)=xdel/sqrt(pe(1,7))
    pe(3,7)=xml-xdel*sqrt(pe(1,7))
    sume=0.  

    do id=1,ndat
      call cal1 (id,ut)
      xt(id,7)=exp(pe(3,7)+pe(2,7)*pe(1,7)*(1.-1./(9*pe(1,7))+ut*sqrt(1./(9.*pe(1,7))))**3.)
      er2(id,7)=(xt(id,7)-xord(id))**2.
      sume=sume+er2(id,7)
    end do  
       
    eea(7)=sqrt(sume/(ndat-3.))

    do it=1,ntr
      call cal2 (it,ut)
      xtt(it,7)=exp(pe(3,7)+pe(2,7)*pe(1,7)*(1.-1./(9*pe(1,7))+ut*sqrt(1./(9.*pe(1,7))))**3.)
    end do


!***Normal 2P***************************************
    pe(1,8)=xmed
    pe(2,8)=xdest
    sume=0.
    
    do id=1,ndat
      call cal1 (id,ut)
      xt(id,8)=pe(1,8)+pe(2,8)*ut
      er2(id,8)=(xt(id,8)-xord(id))**2.
      sume=sume+er2(id,8)
    end do 
    
    eea(8)=sqrt(sume/(ndat-2.))
    
    do it=1,ntr
      call cal2 (it,ut)
      xtt(it,8)=pe(1,8)+pe(2,8)*ut    
    end do   
!***************************************************************************
!***Momentos L**************************************************************   
   
    write(2,*)'Termina c�lculo de par�metros por el m�todo de momentos'
    
    end subroutine Momentos
    
    
    
        
!******************************************************************************************************    
   subroutine cal1 (id,uc)
   use modulo1   
   use modulo2   
    
   implicit real*8 (a-h,o-z)
    
   uc=0.
   if (pexc(id).le.0.5)then
     fx=pexc(id)
     un=sqrt(log(1/fx**2)) 
     uc=un-((2.515517+0.802853*un+0.010328*un**2)/(1+1.432788*un+0.189269*un**2+0.001308*un**3))
   else
     fx=1-pexc(id)
     un=sqrt(log(1/fx**2))
     uc=-(un-((2.515517+0.802853*un+0.010328*un**2)/(1+1.432788*un+0.189269*un**2+0.001308*un**3)))
   end if
   
   end  subroutine
   
   subroutine cal2 (it,ue)
   use modulo1   
   use modulo2   
    
   implicit real*8 (a-h,o-z)
    
   ue=0.
   if (pexe(it).le.0.5)then
     fx=pexe(it)
     un=sqrt(log(1/fx**2)) 
     ue=un-((2.515517+0.802853*un+0.010328*un**2)/(1+1.432788*un+0.189269*un**2+0.001308*un**3))
   else
     fx=1-pexe(it)
     un=sqrt(log(1/fx**2))
     ue=-(un-((2.515517+0.802853*un+0.010328*un**2)/(1+1.432788*un+0.189269*un**2+0.001308*un**3)))
   end if
   
   end  subroutine