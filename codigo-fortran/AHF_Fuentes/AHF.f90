!****************************************************************************
!
!  Programa:    FDP
!  Subrutina:   FDP
!  Comentarios: Estructura del programa principal 
!  Autor:       Jos� Luis Arag�n Hern�ndez
!  FechaIni:    Abr/2016
!  FechaFin:    Abr/2016
!
!****************************************************************************

    program FDP
    
    use moduloDatGen 

    implicit real*8 (a-h,o-z)

    include 'AHF_Commons.txt' 
    
    integer tmpday, tmpmonth, tmpyear
    integer tmphour,tmpminute,tmpsecond,tmphund  
        
    call getdat(tmpyear,tmpmonth,tmpday)
	call gettim(tmphour, tmpminute, tmpsecond, tmphund) 
    open(1,file='Proceso.res',status='unknown')                               !Archivo con informaci�n del proceso  
    
    write(1,*)'*******************************************************'  
    write(1,*)'An�lisis Hidrol�gico de Frecuencias (AHF) V 0.1 (Beta)'
    write(1,*)'Autor: Dr. Jos� Luis Arag�n Hern�ndez'
    write(1,*)'Profesor de Carrera'
    write(1,*)'Departamento de Hidr�ulica, DICyG, Facultad de Ingenier�a, UNAM'
    write(1,*)'Email: jaragonh@unam.mx'
    write(1,1)tmpday,tmpmonth,tmpyear
    write(1,*)'********************************************************' 
    write(1,*)
    write(1,*)'Inicia c�lculos'
    
!***Cuerpo del programa

    open(4, file='Ruta.dat')
    read (4,*,end=4) ruta

    open(2,file='ExtrapolaciondeDatos.res',status='unknown')                             
    write(2,*)'*******************************************************'  
    write(2,*)'An�lisis Hidrol�gico de Frecuencias (AHF) V 0.1 (Beta)'
    write(2,*)'Autor: Dr. Jos� Luis Arag�n Hern�ndez'
    write(2,*)'Profesor de Carrera'
    write(2,*)'Departamento de Hidr�ulica, DICyG, Facultad de Ingenier�a, UNAM'
    write(2,*)'Email: jaragonh@unam.mx'
    write(2,1)tmpday,tmpmonth,tmpyear
    write(2,2)tmphour,tmpminute,tmpsecond
    write(2,*)'********************************************************' 
    write(2,*)
       
    open(3,file='MuestrasSinteticas.res',status='unknown')                             
    write(3,*)'*******************************************************'  
    write(3,*)'An�lisis Hidrol�gico de Frecuencias (AHF) V 0.1 (Beta)'
    write(3,*)'Autor: Dr. Jos� Luis Arag�n Hern�ndez'
    write(3,*)'Profesor de Carrera'
    write(3,*)'Departamento de Hidr�ulica, DICyG, Facultad de Ingenier�a, UNAM'
    write(3,*)'Email: jaragonh@unam.mx'
    write(3,1)tmpday,tmpmonth,tmpyear
    write(3,2)tmphour,tmpminute,tmpsecond
    write(3,*)'********************************************************' 
    
    
    write(1,1)tmpday,tmpmonth,tmpyear
    write(1,2)tmphour,tmpminute,tmpsecond
    write(1,*)
    
!***Licencia**********************************************************    
   im=12.
   ia=2018
   if (tmpmonth.gt.im .or. tmpyear.gt.ia) then
     print*,'***Licencia vencida***'
     write(1,*)'***Licencia vencida***'
     write(2,*)'***Licencia vencida***'
     stop
   end if
!**********************************************************************    

!   Cuerpo del programa
    write(1,*)'Llama subrutinas'
    call LeeDatos
    call ParametrosEstadisticos
    call PruebasdeCalidad
    if (iev.ne.0) call Momentos
    if (nms.gt.0) call SerieSintetica
    call EscribeDatos
    
!   Fin de c�lculos  
    write(1,*)
    write(1,*)'Termina c�lculos'     
    call getdat(tmpyear,tmpmonth,tmpday)
	call gettim(tmphour, tmpminute, tmpsecond, tmphund)     
    write(1,1)tmpday,tmpmonth,tmpyear
    write(1,2)tmphour,tmpminute,tmpsecond    
    
!   Cierre de archivos
    close (1)
    close (2)
    close (3)
    
!   Formatos
1	format(' ',i2.2,'/',i2.2,'/',i4.2)         
2	format(' ',i2.2,':',i2.2,':',i2.2)

4   close (103)

    print *, 'Hello World'
    
    end program FDP

