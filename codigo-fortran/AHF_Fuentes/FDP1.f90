!****************************************************************************
!
!  Programa:    FDP
!  Subrutina:   FDP
!  Comentarios: Estructura del programa principal 
!  Autor:       Jos� Luis Arag�n Hern�ndez
!  FechaIni:    Abr/2016
!  FechaFin:    Abr/2016
!
!****************************************************************************

    program FDP

    implicit real*8 (a-h,o-z)
    
    integer tmpday, tmpmonth, tmpyear
    integer tmphour,tmpminute,tmpsecond,tmphund
    
!   Cuerpo del programa
    open(1,file='Detalles.res',status='unknown')                              !Archivo con informaci�n de los c�lculos
    write(1,*)'*******************************************************'  
    write(1,*)'An�lisis Hidrol�gico de Frecuencias (AHF) V 0.1 (Beta)'
    write(1,*)'Autor: Dr. Jos� Luis Arag�n Hern�ndez'
    write(1,*)'Profesor de Carrera'
    write(1,*)'Departamento de Hidr�ulica, DICyG, Facultad de Ingenier�a, UNAM'
    write(1,*)'Email: jaragonh@unam.mx'
    write(1,*)'Fecha: Abril/2016'
    write(1,*)'********************************************************' 
    write(1,*)   

    open(2,file='Proceso.res',status='unknown')                               !Archivo con informaci�n del proceso hidr�ulico  
    
    write(2,*)'*******************************************************'  
    write(2,*)'An�lisis Hidrol�gico de Frecuencias (AHF) V 0.1 (Beta)'
    write(2,*)'Autor: Dr. Jos� Luis Arag�n Hern�ndez'
    write(2,*)'Profesor de Carrera'
    write(2,*)'Departamento de Hidr�ulica, DICyG, Facultad de Ingenier�a, UNAM'
    write(2,*)'Email: jaragonh@unam.mx'
    write(2,*)'Fecha:Abril/2016'
    write(2,*)'********************************************************' 
    write(2,*)
    write(2,*)'Inicia c�lculos'

	call getdat(tmpyear,tmpmonth,tmpday)
	call gettim(tmphour, tmpminute, tmpsecond, tmphund)     
    write(2,1)tmpday,tmpmonth,tmpyear
    write(2,2)tmphour,tmpminute,tmpsecond
    write(2,*)
    
!***Licencia**********************************************************    
   im=7.
   ia=2016
   if (tmpmonth.gt.im .and. tmyear.gt.ia) then
     print*,'***Licencia caducada***'
     write(1,*)'***Licencia caducada***'
     write(2,*)'***Licencia caducada***'
     stop
   end if
!**********************************************************************    

!   Cuerpo del programa
    write(2,*)'Llama subrutinas'
    call LeeDatos
    call PruebasdeCalidad
    call Momentos
    call EscribeDatos
    
!   Fin de c�lculos  
    write(2,*)
    write(2,*)'Termina c�lculos'     
    call getdat(tmpyear,tmpmonth,tmpday)
	call gettim(tmphour, tmpminute, tmpsecond, tmphund)     
    write(2,1)tmpday,tmpmonth,tmpyear
    write(2,2)tmphour,tmpminute,tmpsecond    
    
!   Cierre de archivos
    close (1)
    close (2)
    
!   Formatos
1	format(' ',I2.2,'/',I2.2,'/',I4.2)         
2	format(' ',I2.2,':',I2.2,':',I2.2)

    print *, 'Hello World'
    
    end program FDP

