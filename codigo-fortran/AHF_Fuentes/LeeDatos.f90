!****************************************************************************
!
!  Programa:    FDP
!  Subrutina:   LeeDatos
!  Comentarios: Leer muestra y periods de retorno
!  Autor:       Jos� Luis Arag�n Hern�ndez
!  FechaIni:    Abr/2016
!  FechaFin:    Abr/2016
!
!****************************************************************************

    subroutine LeeDatos 
        
    use moduloLeeDat  
    use moduloDatGen       
    
    implicit real*8 (a-h,o-z)
    include 'AHF_Commons.txt' 
        
    write(1,*)'Inicia lectura de datos'
    
           
!   Abrir archivos de datos 
    open(101,file='Muestra.dat',status='unknown')                      !Muestra
    open(102,file='PeriodosdeRetorno.dat',status='unknown')            !Muestra
      
!   Lectura de archivos   
    read (101,*,end=101)ndat,iev,nms
    read (102,*,end=102)ntr
    
!***Licencia**********************************************************    
   if (ndat.gt.20) then
     print*,'***Versi�n de prueba: n>20***'
     write(1,*)'***Versi�n de prueba: n>20***'
     write(2,*)'***Versi�n de prueba: n>20***'
     stop
   end if
!**********************************************************************      

    allocate (xori(ndat),test(ntr),pexe(ntr))
    xori=0.
    test=0.
    pexe=0.
    
    !Lectura de datos           
    do id=1,ndat
      read (101,*,end=101) xori(id)
      if (xori(id).le.0.) then
        write(2,1)'Valor de la serie menor o igual que cero:',xori(id)
        xori(id)=1.
      end if
    end do  

    do it=1,ntr
      read (102,*,end=102) test(it)
      if (test(it).le.1) test(it)=1.1
      pexe(it)=(1./test(it)) 
    end do  
    
!   Cierre de archivos de datos 
101   close (101)
102   close (102)
1   format(A,F10.3)
    write(1,*)'Termina lectura de datos'

    end subroutine LeeDatos