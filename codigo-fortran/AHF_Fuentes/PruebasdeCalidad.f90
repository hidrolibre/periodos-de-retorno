!****************************************************************************
!
!  Programa:    FDP
!  Subrutina:   Pruebas de calidad
!  Comentarios: Se aplican pruebas algunas pruebas de calidad a la serie
!  Autor:       Jos� Luis Arag�n Hern�ndez
!  FechaIni:    Abr/2016
!  FechaFin:    Abr/2016
!
!****************************************************************************

    subroutine PruebasdeCalidad 
        
    use moduloLeeDat
    use moduloParEst         
    use moduloPruCal   

    
    implicit real*8 (a-h,o-z)
    include 'AHF_Commons.txt' 

    character(len=100)peh,pei,pein,rpes,rpec,res
    allocate (isc(ndat),rk(nk),rk1(nk),rk2(nk),rkn(nk),rkn1(nk),rkn2(nk))
    isc=0.
    rk=0.
    rk1=0.
    rk2=0.
    rkn=0.
    rkn1=0.
    rkn2=0.
    
    write(1,*)'Inicia c�lculo de pruebas de calidad' 

!***Pruebas de calidad***************************************    
    write (2,*)
    write (2,*)'Pruebas de calidad'
    
!***Pruebas estad�sticas de hom�geneidad***************************************    
!***Prueba de Helmert   
    write (1,*)'Pruebas estad�stica de Helmert'

    do id=1,ndat
      if (xori(id)-xmed.ge.0.) then
        smed(id)=1.
      else
        smed(id)=-1.
      end if
    end do  
    iss=0.
    icc=0.
    do id=2,ndat
      if (smed(id-1)-smed(id).eq.0.) then
        isc(id)=1.
        iss=iss+1.
      else
        isc(id)=-1.
        icc=icc+1.
      end if
    end do    
    aux1=abs(iss-icc)
    aux2=sqrt(ndat-1.)
    
    nres=0
    if (aux1.le.aux2 .and. aux1.ge.-aux2) then
      peh='Hom�genea'
      nres=nres+1
    else
      peh='No hom�genea'
      nres=nres-1
    end if
    
    write(2,1)' Prueba estad�stica de Helmert: ',peh
    write(2,2)xori(1),xmed,smed(1)
    do id=2,ndat
      if (isc(id).eq.1) then
        write(2,3)xori(id),xmed,smed(id),'s'
      else
        write(2,3)xori(id),xmed,smed(id),'c' 
      end if
    end do
    write(2,4)' Secuencias:',iss
    write(2,4)' Cambios:',icc
    
!***Prueba de Cramert  
    write (1,*)'Prueba estad�stica t de Student'
    
    ndat1=nint(0.5*ndat)
    ndat2=ndat1+1
    
    ic1=0
    sumxnd1=0
    do id=1,ndat1
      ic1=ic1+1
      sumxnd1=sumxnd1+xori(id)
    end do  
    xmed1=sumxnd1/ic1 
    
    ic2=0  
    sumxnd2=0
    do id=ndat2,ndat
      ic2=ic2+1 
      sumxnd2=sumxnd2+xori(id)
    end do  
    xmed2=sumxnd2/ic2
    
    sumxnd12=0   
    do id=1,ndat1
      xxm(id)=(xori(id)-xmed)
      sumxnd12=sumxnd12+(xori(id)-xmed)**2.
    end do 
    xvar1=sumxnd12/(ici-1)   
    
    sumxnd22=0   
    do id=ndat2,ndat
      xxm(id)=(xori(id)-xmed)
      sumxnd22=sumxnd22+(xori(id)-xmed)**2.
    end do 
    xvar2=sumxnd22/(ic2-1)     
 
    pes=(xmed1-xmed2)/((ic1*xvar1+ic2*xvar2)/(ic1+ic2-2)*(1./ic1+1./ic2))**0.5    
    
    igl=ic1+ic2-2
    fct1=(igl+1)/2.-1
    fct2=igl/2.-1
    
    call funcionGamma(fct1,fgm1) 
    call funcionGamma(fct2,fgm2)
    alfa=0.05  
    !pts=fgm1/(sqrt(pi*igl)*fgm2)*(1+alfa**2./igl)**(-(igl+1)/2.)  !Revisar el calcaulo de t de estudente de 2 colas
    gl=ic1+ic2-2
    pts=ftds(gl)
    !iresfact=ifac(3)

    if (pes.le.pts) then
      rpes='Hom�genea'
      nres=nres+1
    else
      rpes=' No hom�genea'
      nres=nres-1
    end if
    
   write(2,1)' Prueba estad�stica t de Student: ',rpes
   write(2,6)' td',pes
   write(2,6)' ts',pts


!***Prueba estad�stica de Cramert  
    write (1,*)'Prueba estad�stica de Cramer'
 
    ndat1=ndat-nint(0.6*ndat)+1
    ndat2=ndat-nint(0.3*ndat)+1
    
    ic1=0
    ic2=0
    sumxnd1=0
    do id=ndat1,ndat
      ic1=ic1+1
      sumxnd1=sumxnd1+xori(id)
    end do  
    xmed1=sumxnd1/ic1  
    sumxnd2=0
    do id=ndat2,ndat
      ic2=ic2+1
      sumxnd2=sumxnd2+xori(id)
    end do  
    xmed2=sumxnd2/ic2
    
    tw1=(xmed1-xmed)/xdest
    tw2=(xmed2-xmed)/xdest
    
    pec1=sqrt(ic1*(ndat-2)/(ndat-ic1*(1+tw1**2.)))*abs(tw1)
    pec2=sqrt(ic2*(ndat-2)/(ndat-ic2*(1+tw2**2.)))*abs(tw2)

    gl=ic1+ic2-2
    pts=ftds(gl)

    if (pec1.le.pts .and. pec2.le.pts) then
      rpec='Hom�genea'
      nres=nres+1      
    else
      rpec=' No hom�genea'
      nres=nres-1
    end if
    
   write(2,1)' Prueba estad�stica de Cramer: ',rpec
   write(2,6)' tw1',pec1
   write(2,6)' tw2',pec2
   write(2,6)' ts ',pts
   
  !Resultado
  if (nres.gt.0)then
    res='Hom�genea' 
  else
    res=' No hom�genea'
  end if
  write(2,1)' Pruebas de homogeneidad: ',res
    
!***Prueba estad�stica de Independencia*****         
!***Prueba de Anderson*****      
    ic1=0.
    ic2=0
    
    do ik=1,nk
        sumxxm=0.    
      do id=1,ndat-ik
        sumxxm=sumxxm+(xxm(id)*xxm(id+ik))
      end do
      rk(ik)=sumxxm/sumx2
      rk1(ik)=(-1.+1.96*sqrt(ndat-ik-1.))/(ndat-ik)
      rk2(ik)=(-1.-1.96*sqrt(ndat-ik-1.))/(ndat-ik)
      if (rk(ik).le.rk1(ik) .and. rk(ik).ge.rk2(ik) ) then
        ic1=ic1+1.
      else
        ic2=ic2+1.
      end if  
    end do
    
    if (ic1/nk.ge.0.9) then
      pei='Independiente'
    else
      pei='Dependiente'
    end if    
     
    write (2,*)
    write(2,1)' Prueba estad�stica de Anderson: ',pei
    
    write(2,*)'    rk(+95)          rk     rk(-95)'
    do ik=1,nk
      write (2,5)rk1(ik),rk(ik),rk2(ik) 
    end do
    
!***Prueba de Anderson:Serie normalizada*****    
    sumxxmn=0.  
    ic1=0.
    ic2=0
    
    do ik=1,nk
      do id=1,ndat-ik
      sumxxmn=sumxxmn+(xxmn(id)*xxmn(id+1))
      end do
      rkn(ik)=sumxxmn/sumxn2
      rkn1(ik)=(-1.+sqrt(ndat-ik-1.))/(ndat-ik)
      rkn2(ik)=(-1.-sqrt(ndat-ik-1.))/(ndat-ik)
      if (rkn(ik).le.rkn1(ik) .and. rkn(ik).ge.rkn2(ik) ) then
        ic1=ic1+1.
      else
        ic2=ic2+1.
      end if  
    end do
    
    if (ic1/nk.ge.0.9) then
      pein='Independiente'
    else
      pein='Dependiente'
    end if    
     
1   format(a,a)
2   format(2f12.3,i7)
3   format(2f12.3,i7,a6)
4   format(a,i2)
5   format(20f12.3)
6   format(a,f12.3)

    write(1,*)'Termina c�lculo de pruebas de calidad'

    end subroutine PruebasdeCalidad
   
!******************************************************
    function ifac(ifct)
    implicit real*8 (a-h,o-z)
    
    ifac=1
    do ift=2,ifct
      ifac=ifac*ift
    end do
    
    end function ifac
    
!******************************************************
    function ftds(gl1)
    use moduloftds
    
    implicit real*8 (a-h,o-z)
    
    open(201,file='tds.dat',status='unknown')                     
    read (201,*,end=201)ndat
    
    allocate(igl(ndat),tds(ndat))
    igl=0
    tds=0
     
    do idat=1,ndat
      read (201,*,end=201)igl(idat),tds(idat)
      if (igl(idat).eq.gl1) then
        ftds=tds(idat)
        exit
      end if  
    end do
    
    if (gl1.ge.30 .and. gl1.le.40) then
      ftds=0.0021*gl1+1.958
    else if (gl1.ge.40 .and. gl1.le.60) then
      ftds=0.00105*gl1+1.958
    else if (gl1.ge.60 .and. gl1.le.120) then
      ftds=0.0003333*gl1+1.96
    else if (gl1.gt. 120) then
      ftds=0.0000228*gl1+1.95727 
    else
      ftds=1.96      
    end if

201 close (201)

    deallocate (igl,tds)
        
    end function ftds