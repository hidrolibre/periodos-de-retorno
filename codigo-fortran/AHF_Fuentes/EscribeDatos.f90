!****************************************************************************
!
!  Programa:    FDP
!  Subrutina:   Escribe datos
!  Comentarios: 
!  Autor:       Jos� Luis Arag�n Hern�ndez
!  FechaIni:    Abr/2016
!  FechaFin:    Abr/2016
!
!****************************************************************************

    subroutine EscribeDatos 
    
    use moduloLeeDat
    use moduloParEst         
    use moduloPruCal   
    use moduloFDP
    
    implicit real*8 (a-h,o-z)
    include 'AHF_Commons.txt' 
    
    character(len=50)nomfdp
    character(len=150)tit,cc
  
    write(1,*)'Inicia escritura de datos'
    
    eeamin=1000000
    do ifdp=1,nf
      if (eea(ifdp).lt.eeamin) then
        eeamin=eea(ifdp)
        nfdp=ifdp
      end if
    end do
    
    if (nfdp.eq.1.) then
      nomfdp='Gamma de 2P por Momentos'
    else if (nfdp.eq.2.) then
      nomfdp='Gamma de 3P por Momentos' 
    else if (nfdp.eq.3.) then
      nomfdp='General de valores extremos de 3P por Momentos'      
    else if (nfdp.eq.4.) then
      nomfdp='Gumbel de 2P por Momentos' 
    else if (nfdp.eq.5.) then
      nomfdp='LogNormal de 2P por Momentos'       
    else if (nfdp.eq.6.) then
      nomfdp='LogNormal de 3P por Momentos'   
    else if (nfdp.eq.7.) then
      nomfdp='LogPearson III de 2P por Momentos'      
    else if (nfdp.eq.8.) then
      nomfdp='Normal de 2P por Momentos'       
    end if
           
!   Abre archivos para escritura   
    open(1002,file='Leeme.txt',status='unknown')                      !Muestra

    write(1002,*)'*******************************************************'  
    write(1002,*)'An�lisis Hidrol�gico de Frecuencias (AHF) V 0.1 (Beta)'
    write(1002,*)'Autor: Dr. Jos� Luis Arag�n Hern�ndez'
    write(1002,*)'Profesor de Carrera'
    write(1002,*)'Departamento de Hidr�ulica, DICyG, Facultad de Ingenier�a, UNAM'
    write(1002,*)'Email: jaragonh@unam.mx'
    write(1002,*)'Fecha: Abril/2016'
    write(1002,*)'********************************************************' 
    write(1002,*)   
    write(1002,*)'Los archivos se conforman como:'
    write(1002,*)
    write(1002,*)'PeriodosdeRetorno.dat'  
    write(1002,*)'m    No. de periodos de retorno'     
    write(1002,*)'T1   Valor de los periodos de retorno'   
    write(1002,*)'T2'
    write(1002,*)'...'
    write(1002,*)'Tm'  
    write(1002,*)
    write(1002,*)'Muestra.dat'   
    write(1002,*)'n    No. de datos de la muetra'   
    write(1002,*)'x1   Datos de la muestra'   
    write(1002,*)'x2'   
    write(1002,*)'...'   
    write(1002,*)'xn'   
    write(1002,*)'****************************************************'   
    write(1002,*)   
    write(1002,*)'Citar como:'   
    cc=' Arag�n-Hern�ndez, J. L. (2016). An�lisis Hidrol�gico de Frecuencias.' 
    write(1002,6),cc
    write(1002,*) 'Nota: Domentaci�n e interfaz en desarrollo. ' 
   
    !Escribe datos 
    write (2,*)
    write (2,*)'Datos del ajuste'
    write (2,4)' FDP con mejor ajuste: ',nomfdp
    write (2,2)' EAA:              ',eeamin
    write (2,5)' Par�metros:     ',pe(1:3,nfdp)
    
    write (2,*)
    write (2,*) '      xorig        xord          Tr        xcal      error2'  
    do id=1,ndat
      write (2,1) xori(id),xord(id),tcal(id),xt(id,nfdp),er2(id,nfdp)  
    end do
    write (2,2)

    tit='          Tr    Gamm2PxM    Gamm3PxM     GVE2PxM    Gumb2PxM    LNor2PxM    LNor3PxM   LPIII2PxM    Norm2PxM' 
    write(2,6),tit
    do it=1,ntr
      write (2,1) test(it),xtt(it,1:nf) 
    end do
    write(2,5)'         EEA',eea(1:nf)
     
!   Cierra archivos de datos 
1001 close (1001)

1	format(20f12.3)
2	format(a,f10.3)
3	format(a,i2)
4	format(50a,50a)
5	format(a,20f12.3)
6   format(10a)
7   format(20f12.3)

    write(1,*)'Termina escritura de datos'

    end subroutine EscribeDatos