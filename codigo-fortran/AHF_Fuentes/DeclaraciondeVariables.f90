!****************************************************************************
!
!  Programa:    FDP
!  Subrutina:   Declaraci�n de variables
!  Comentarios: Declarar todas las variables 
!  Autor:       Jos� Luis Arag�n Hern�ndez
!  FechaIni:    Abr/2016
!  FechaFin:    Abr/2016
!
!****************************************************************************
    module moduloDatGen
      character*260 ruta
    end module moduloDatGen
    
    module moduloLeeDat
      real*8,save,allocatable:: xori(:),pexe(:),test(:)
      !character, allocatable:: ruta
    end module moduloLeeDat
       
    module moduloParEst
      real*8,save,allocatable:: xord(:),tcal(:),pexc(:),xn(:),xxm(:),xxmn(:)
      integer,save,allocatable::smed(:)
    end module moduloParEst 
            
    module moduloPruCal
      real*8,save,allocatable::rk(:),rk1(:),rk2(:),rkn(:),rkn1(:),rkn2(:)
      integer,save,allocatable::isc(:)
    end module moduloPruCal
    
    module moduloFDP
      real*8,save,allocatable:: xt(:,:),xtt(:,:),er2(:,:),eea(:),pe(:,:)
    end module moduloFDP
    
    module moduloSerSin
      real*8,save,allocatable:: zt(:,:),yt(:,:),xs(:,:),xmeds(:),xvars(:),xdests(:),cass(:)
    end module moduloSerSin
    
    module moduloftds
      real*8,save,allocatable:: tds(:)
      integer,save,allocatable:: igl(:)
    end module moduloftds