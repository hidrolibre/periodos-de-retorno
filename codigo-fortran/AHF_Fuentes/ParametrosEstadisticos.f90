!****************************************************************************
!
!  Programa:    FDP
!  Subrutina:   Pruebas de calidad
!  Comentarios: Se aplican pruebas algunas pruebas de calidad a la serie
!  Autor:       Jos� Luis Arag�n Hern�ndez
!  FechaIni:    Abr/2016
!  FechaFin:    Abr/2016
!
!****************************************************************************

    subroutine ParametrosEstadisticos 
    
    use moduloLeeDat    
    use moduloParEst         
    use moduloPruCal     
    
    implicit real*8 (a-h,o-z)
    include 'AHF_Commons.txt' 
        
    write(1,*)'Inicia c�lculo de par�metros estad�sticos'
    nk=ndat/3.
    allocate (xord(ndat),tcal(ndat),pexc(ndat),xn(ndat),smed(ndat),xxm(ndat),xxmn(ndat))
    xord=xori
    tcal=0.
    pexc=0.
    xn=0.
    smed=0.
    xxm=0.
    xxmn=0.
    
!***Par�metros estad�sticos***** 
    sumx=0
    do id=1,ndat
      sumx=sumx+xori(id)
      xn(id)=log(xori(id))
      sumxn=sumxn+xn(id)
    end do  
    xmed=sumx/ndat
    xmedn=sumxn/ndat
    sumx2=0
    sumx3=0
    sumxn2=0
    sumxn3=0    
    do id=1,ndat
      xxm(id)=(xori(id)-xmed)
      xxmn(id)=(xn(id)-xmedn)
      sumx2=sumx2+(xori(id)-xmed)**2.
      sumx3=sumx3+(xori(id)-xmed)**3.
      sumxn2=sumxn2+(xn(id)-xmedn)**2.
      sumxn3=sumxn3+(xn(id)-xmedn)**3.
    end do 
    xvar=sumx2/(ndat-1.)   
    xdest=sqrt(xvar)
    cas=(sumx3/ndat)/(sumx2/ndat)**1.5 
    xvarn=sumxn2/(ndat-1.)
    xdestn=sqrt(xvarn)
    casn=(sumxn3/ndat)/(sumxn2/ndat)**1.5 

    write(2,*)'Par�metros estad�sticos de la muestra'
    write(2,1)' Media:                     ',xmed
    write(2,1)' Varianza:                  ',xvar
    write(2,1)' Desviaci�n est�ndar:       ',xdest
    write(2,1)' Coeficiente de asimetr�a:  ',cas

!***Ordena de mayor a menor***** 
    aux1=0.
    do id=1,ndat-1
      do id1=1,ndat-1
        if (xord(id1).lt.xord(id1+1)) then
          aux1=xord(id1)
          xord(id1)=xord(id1+1)
          xord(id1+1)=aux1
        end if
      end do     
    end do
      
 !***Asigna periodo de retorno*****   
    do id=1,ndat
      tcal(id)=(ndat+1.)/id
      pexc(id)=1/tcal(id)
    end do  
   
1	format(a,20f12.3)

    write(1,*)'Termina c�lculo de par�metros estad�ticos'

    end subroutine ParametrosEstadisticos

