"""Este módulo realiza las pruebas de calidad a la muestra"""

def helmert(muestra : list) -> float:
    """Realiza la prueba estadística de Helmert e indica si es homogénea, el número de secuencias y el número de cambios"""
    xmed = xmed(muestra)
    muestra2 = list(muestra)
    del muestra2[0]
    iss = 0
    icc = 0
    aux1 = 0
    aux2 = sqrt(len(muestra)-1)
    nres = 0
    
    for xi, xi2 in zip(muestra, muestra2):
        if xi-xmed > 0:
            smed = 1
            else:
                smed = -1
            return
        
        if xi2-xmed > 0:
            smed2 = 1
            else:
                smed2 = -1
            return
        if smed-smed2 = 0:
            print(xi, 's')
            iss = iss+1
            else:
                print(xi, 'c')
                icc = icc+1
            return
        return
    
    aux1 = abs(iss-icc)
    
    if aux1<aux2 and aux1>-aux2:
        peh = 'Homogénea'
        nres = 1
        else:
            peh = 'No homogénea'
            nres = -1
        return
    return nres, iss, icc

