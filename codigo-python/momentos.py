"""Este módulo immplementa las funciones de ajustes por momentos"""

def gamma2p(muestra : list, periodo : float) -> float:
	"""Calcula el evento de diseño ajustandolo a la distribución y regresa
	(xtt,error) donde xtt es el evento de diseño y error es la incertidumbre
	que se tiene con el ajuste"""
	media = sum(muestra)/len(muestra)
	s = desviacion_estandar(muestra)
	alfa = s**2/media
	beta = (media/s)**2
	errores = list()
	
	for i,dato in enumerate(sorted(muestra)):
		#De momento no se tiene a la subrutina en python cal1 '(id,uc)'
		xt = alfa*beta*(1-1/(9*beta)+ut*sqrt(1/(9*beta)))**3
		errores.insert((xt-dato)**2)
		
	error = swrt(sum(errores)/(len(muestra)-2))
	
	#De momento no se tiene a la subrutina en python 'cal2 (it,ut)'
	xtt = alfa*beta*(1-1/(9*beta)+ut*sqrt(1/(9*beta)))**3
    
	return (xtt,error)
