"""Éste módulo es para mostrar los parámetros estadísticos de la muestra"""

def xmed(muestra : list) -> float:
    """Calcula la media de la muestra"""
    return sum(muestra)/len(muestra)

def xvar(muestra : list,) -> float:
    """Calcula la vairanza de la muestra"""
    xmed = xmed(muestra)
    return sum([(xi-xmed)**2 for xi in muestra])/(len(muestra)-1)

def xdest(muestra : list) -> float:
	"""Calcula la desviación estandar de la muestra"""
    xvar = xvar(muestra)
	return sqrt(xvar)

def cas(muestra : list) -> float:
    """Calcula el coeficiente de asimetría"""
    xmed = xmed(muestra)
    sumx3 = sum([(xi-xmed)**3 for xi in muestra])
    sumx2 = sum([(xi-xmed)**2 for xi in muestra])
    return (sumx3/len(muestra))/((sumx2/len(muestra))**1.5)